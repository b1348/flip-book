import express from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";
import cors from "cors";
import bodyParser from "body-parser";
import CardRoute from "./routes/cardRoute.js";

dotenv.config();
const app = express();
app.use(cors());
app.use(bodyParser.json());

const PORT = process.env.PORT || 2000;
const MONGODB_URI =
  "mongodb+srv://saajidh:saajidh123@cluster0.5gmpn.mongodb.net/finalpaper2021?retryWrites=true&w=majority";

mongoose
  .connect(MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() =>
    app.listen(PORT, () =>
      console.log(`connection established successfully on port: ${PORT}`)
    )
  )
  .catch((err) => console.log(err.message));

app.use("/card", CardRoute);

// import express from 'express';
// import mongoose from 'mongoose';

// import cors from 'cors';
// import bodyParser from 'body-parser';
// const app = express();
// app.use(cors());
// app.use(json());
// const PORT = process.env.PORT || 5000;

// const connect_URI =
//   "mongodb+srv://saajidh:saajidh123@cluster0.5gmpn.mongodb.net/flip-book?retryWrites=true&w=majority";

// app.use("/card", CardRoute);

// connect(connect_URI, {
//   useNewUrlParser: true,
//   useUnifiedTopology: true,
// })
//   .then(() =>
//     app.listen(PORT, () =>
//       console.log(`connection established successfully on port: ${PORT}`)
//     )
//   )
//   .catch((err) => console.log(err.message));
