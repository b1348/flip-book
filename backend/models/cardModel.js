import mongoose from "mongoose";

const CardSchema = new mongoose.Schema({
  cardTitle: {
    type: String,
    required: false,
  },
  cardSubTitle: {
    type: String,
    required: false,
  },
  cardPersonName: {
    type: String,
    required: false,
  },
  images: {
    type: Object,
    required: false,
  },
});
const Card = mongoose.model("Card", CardSchema);
export default Card;
