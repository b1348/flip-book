import express from "express";
import Card from "../models/cardModel.js";
const router = express.Router();
//insert
router.post("/editImage", async (req, res) => {
  console.log("inserted");

  const c = req.body;
  const newCard = new Card(c);
  try {
    await newCard.save();
    res.send({ success: "true", message: "Card Inserted " });
  } catch (e) {
    console.log(e);
  }
});

router.get("/getImage", async (req, res) => {
  try {
    const allImages = await Card.find();
    res.json(allImages);
  } catch (e) {
    console.log(e);
  }
});

// router.put("/updateImage/:id", (req, res) => {
//     console.log("updating....");
//     Card.findByIdAndUpdate(req.params.id, req.body)
//         .then(res.json({ msg: "Updated successfully" }))
//         .catch((err) =>
//             res.status(400).json({ error: "Unable to update the Database" })
//         );
// });

export default router;
