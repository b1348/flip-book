import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import "./App.css";

import Section from "./components/section";

export class App extends Component {
  render() {
    return (
      <Router>
        <Section />
      </Router>
    );
  }
}

export default App;
