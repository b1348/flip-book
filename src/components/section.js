import React, { Component } from "react";
import { Route } from "react-router-dom";
import Home from "./Home/Home";
import EditImage from "./Home/Edit";

class Section extends Component {
  render() {
    return (
      <section>
        <Route path="/" component={Home} exact />
        <Route path="/editImage" component={EditImage} />
      </section>
    );
  }
}

export default Section;
