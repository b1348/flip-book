import React, { Component } from "react";
import Draggable from "react-draggable";
import pic1 from "../images/pic1.jpg";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import { useState, useEffect } from "react";
import axios from "axios";
import MultipleImageUpload from "./MultipleImageUpload";
// import base64 from "base-64";
import FileBase64 from "react-file-base64";

function EditImage() {
  const [editable, setEditable] = useState(false);
  const [card, setCard] = useState({
    cardTitle: "",
    cardSubTitle: "",
    cardPersonName: "",
    images: "",
  });

  // const [myimage, setMyImage] = React.useState(null);
  // const uploadImage = (e) => {
  //   setMyImage(URL.createObjectURL(e.target.files[0]));
  // };

  function updateImage() {
    axios.post("http://localhost:2000/card/editImage", card);
  }

  //  function valueChange(e){
  //     const {textContent} = e.currentTarget;
  //     const {name} = e.target;
  //     setValue(prevValue=> ({
  //         ...prevValue,
  //         [name] : textContent
  //         })
  //     );

  //     console.log(value);
  // }

  return (
    <>
      {/* <div className="App">
        <img
          src={myimage}
          alt="custom-pic"
          style={{ height: 100, width: 100 }}
        />
        <input type="file" onChange={uploadImage} />
        <hr />
        <MultipleImageUpload />
      </div> */}

      <div className="container">
        <div
          className="mx-auto "
          style={{
            border: "0px solid #000",
            width: "350px",
            height: "500px",
            marginTop: "5%",
            borderRadius: "10px",
            backgroundImage: `url(${pic1})`,
          }}
        >
          <Draggable>
            <h1
              style={{
                color: "white",
                fontFamily: "Cursive",
                fontStyle: "italic",
              }}
              contentEditable="true"
              id="cardTitle"
              name="cardTitle"
              onInput={(e) =>
                setCard({ ...card, cardTitle: e.currentTarget.textContent })
              }
            >
              Customize
            </h1>
          </Draggable>
          <p></p>
          <Draggable>
            <h1
              style={{
                color: "white",
                fontFamily: "Cursive",
                fontStyle: "italic",
              }}
              contentEditable="true"
              id="cardSubTitle"
              name="cardSubTitle"
              onInput={(e) =>
                setCard({ ...card, cardSubTitle: e.currentTarget.textContent })
              }
            >
              Your
            </h1>
          </Draggable>
          <p></p>
          <Draggable>
            <h2
              style={{
                color: "white",
                fontFamily: "Cursive",
                fontStyle: "italic",
              }}
              contentEditable="true"
              id="cardPersonName"
              name="cardPersonName"
              onInput={(e) =>
                setCard({
                  ...card,
                  cardPersonName: e.currentTarget.textContent,
                })
              }
            >
              Card here
            </h2>
          </Draggable>
        </div>
        <FileBase64
          multiple={true}
          onDone={(base64) => {
            setCard({ ...card, images: base64 });
          }}
        />
        <button
          type="submit"
          className="btn btn-dark"
          onClick={updateImage}
          style={{ marginLeft: "45%", marginTop: "1%", width: "120px" }}
        >
          Save Book
        </button>
        <div className="mx-auto">
          <Link to="/">
            <Button
              className="btn btn-dark"
              style={{ marginLeft: "45%", marginTop: "1%" }}
            >
              Back to Book
            </Button>
          </Link>
        </div>
      </div>
    </>
  );
}

export default EditImage;
