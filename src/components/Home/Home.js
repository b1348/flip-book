import HTMLFlipBook from "react-pageflip";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
// import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import img01 from "../images/1.jpg";
import img02 from "../images/2.jpg";
import img03 from "../images/3.jpg";
import img04 from "../images/4.jpg";
import img05 from "../images/5.jpg";
import img06 from "../images/6.jpg";

const Page = React.forwardRef((props, ref) => {
  return (
    <div className="demoPage" ref={ref}>
      {" "}
      {/* <h2>Page Header</h2> */}
      <p>{props.children}</p>
      {/* <p>Page number: {props.number}</p> */}
    </div>
  );
});

function Home(props) {
  return (
    <center>
      <Link to="/editImage">
        <Button className="btn btn-dark">Edit Image</Button>
      </Link>
      <p></p>

      {/* <h2>Sample Flip Page</h2> */}
      <HTMLFlipBook width={500} height={500}>
        <Page number="1">
          <img src={img01} />
        </Page>
        <Page number="2">
          <img src={img02} />
        </Page>
        <Page number="3">
          <img src={img03} />
        </Page>
        <Page number="4">
          <img src={img04} />
        </Page>
        <Page number="5">
          <img src={img05} />
        </Page>
        <Page number="6">
          <img src={img06} />
        </Page>
      </HTMLFlipBook>
    </center>
  );
}

export default Home;
